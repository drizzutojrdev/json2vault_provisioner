from os.path import isfile, join
import json
from datetime import datetime
import ast

import engine as engine
import vault_connector as vc

class KVv2(engine.Engine):

  def __init__(self, request_contents, template_path):

    super().__init__(request_contents, template_path)

    self.url_path = "/v1/"+self.request_contents['request_data']['path']+"/data"+self.request_contents['engine_data']['sub_path']+"/"+self.request_contents['request_data']['name']
    self.url_path = self.url_path.replace("//", "/")

    if "template" not in self.request_contents['request_data'].keys():
      self.request_contents['request_data']['template'] = ""


  def check_template(self):
    ### COME BACK to ###
    return True


  def create(self):

    is_valid = self.check_template()

    if not is_valid:
      return self.request_contents

    self.vault_connection = vc.vault_connection(self.request_contents['credentials'])
    self.request_contents['credentials'] = self.vault_connection.get_credentials()
    if not self.vault_connection.generate_token():
      run_time = datetime.now()
      self.request_contents['response'] = {
        "result" : "not applied",
        "success" : False,
        "error" : {
          "vault_message" : self.vault_connection.get_token_error()
        },
        "message" : "Could not generate token",
        "run_date" : run_time.strftime("%Y_%m_%d-%H:%M:%S")
      }
      return self.request_contents

    if self.request_contents['overwrite'] == False:
      if self.check_if_exists(engine="kvv2"):
        run_time = datetime.now()
        self.request_contents['response'] = {
          "result" : "not applied",
          "success" : True,
          "error" : {},
          "message" : "Resource exists, set overwrite to True to replace current resource",
          "run_date" : run_time.strftime("%Y_%m_%d-%H:%M:%S")
        }

        return self.request_contents

    response = self.vault_connection.post_call(
      self.request_contents['request_data']['namespace'], 
      self.url_path,
      self.request_contents['engine_data']['secret']
    )

    run_time = datetime.now()

    if 'errors' in response.json():
      print("Error - Could not create Kv v2 Secrets")
      self.request_contents['response'] = {
        "run_date" : run_time.strftime("%Y_%m_%d-%H:%M:%S"),
        "success" : False,
        "message" : "An Error occurred when creating the resource",
        "error" : {
          "response" : response.text
        }
      }
    else:
      self.request_contents['response'] = {
        "run_date" : run_time.strftime("%Y_%m_%d-%H:%M:%S"),
        "success" : True,
        "message" : "Secret Created",
        "error" : {},
        "vault_response" : response.json()
      }
    return self.request_contents


  def update(self):
    run_time = datetime.now()
    self.request_contents['response'] = {
      "result" : "not applied",
      "success" : False,
      "error" : {},
      "message" : "WORK IN PROGRESS",
      "run_date" : run_time.strftime("%Y_%m_%d-%H:%M:%S")
    }

    return self.request_contents
