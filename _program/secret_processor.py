import kvv2_engine as kvv2e
import ssh_engine as ssh
from datetime import datetime

class secret_processor:

  def __init__(self, request_contents, template_path):
    self.request_contents = request_contents
    self.template_path = template_path


  def process_secret_request(self):

    if self.request_contents['request_data']['engine'].lower() == "kvv2":
      new_kvv2_secret = kvv2e.KVv2(self.request_contents, self.template_path)
      return new_kvv2_secret.take_action()
    elif self.request_contents['request_data']['engine'].lower() == "ssh":
      new_ssh_role = ssh.SSH(self.request_contents, self.template_path)
      return new_ssh_role.take_action()
    else:
      run_time = datetime.now()
      self.request_contents['response'] = {
        "result" : "not applied",
        "success" : False,
        "error" : {
          "response" : "The Secret Engine does not have a solution in place"
        },
        "message" : "Program cannot handle secrets of type " + self.request_contents['request_info']['engine'],
        "run_date" : run_time.strftime("%Y_%m_%d-%H:%M:%S")
      }

    return self.request_contents