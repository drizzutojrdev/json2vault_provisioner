from os.path import isfile, join
import json
from datetime import datetime
import ast

import engine as engine
import vault_connector as vc

class Approle(engine.Engine):

  def __init__(self, request_contents, template_path):

    super().__init__(request_contents, template_path)

    self.url_path = "/v1/auth/"+self.request_contents['request_data']['path']+"/role/"+self.request_contents['request_data']['name']
    self.url_path = self.url_path.replace("//", "/")

    if "template" not in self.request_contents['request_data'].keys():
      self.request_contents['request_data']['template'] = "default_approle.json"


  def check_template(self):
    if isfile(join(self.template_path, self.request_contents['request_data']['template'])):
      template_data = json.load(open(join(self.template_path, self.request_contents['request_data']['template'])))

      for key in self.request_contents['engine_data'].keys():
        if key in ["secret_id_bound_cidrs", "token_policies", "token_bound_cidrs"]:
          template_data[key] = self.request_contents['engine_data'][key] + template_data[key]
        else:
          template_data[key] = self.request_contents['engine_data'][key]

      self.request_contents['engine_data'] = template_data

      return True
    else:
      run_time = datetime.now()
      self.request_contents['response'] = {
        "result" : "not applied",
        "success" : False,
        "error" : {
          "response" : "template " + self.request_contents['request_data']['template'] + " not found in " + self.template_path
        },
        "message" : "Template Error",
        "run_date" : run_time.strftime("%Y_%m_%d-%H:%M:%S")
      }

    return False


  def update(self):
    print("Update")
    is_valid = True

    self.vault_connection = vc.vault_connection(self.request_contents['credentials'])
    self.request_contents['credentials'] = self.vault_connection.get_credentials()
    if not self.vault_connection.generate_token():
      run_time = datetime.now()
      self.request_contents['response'] = {
        "result" : "not applied",
        "success" : False,
        "error" : {
          "vault_message" : self.vault_connection.get_token_error()
        },
        "message" : "Could not generate token",
        "run_date" : run_time.strftime("%Y_%m_%d-%H:%M:%S")
      }
      return self.request_contents

    approle_exists = self.check_if_exists()

    if not approle_exists:
      run_time = datetime.now()
      self.request_contents['response'] = {
        "result" : "not applied",
        "success" : True,
        "error" : {},
        "message" : "Approle does not exist, cannot update approle that does not exist",
        "run_date" : run_time.strftime("%Y_%m_%d-%H:%M:%S")
      }
      return self.request_contents

    for key in self.request_contents['engine_data'].keys():
      approle_exists['data'][key] = self.request_contents['engine_data'][key]

    approle_exists['data'].pop('local_secret_ids', None)

    response = self.vault_connection.post_call(
      self.request_contents['request_data']['namespace'], 
      self.url_path,
      approle_exists['data']
    )
    run_time = datetime.now()

    if response.text == "":
      self.request_contents['response'] = {
        "run_date" : run_time.strftime("%Y_%m_%d-%H:%M:%S"),
        "success" : True,
        "message" : "Approle Updated",
        "error" : {}
      }

    else:
      print("Error - Could not create Approle")
      self.request_contents = {
        "run_date" : run_time.strftime("%Y_%m_%d-%H:%M:%S"),
        "success" : False,
        "message" : "An Error occurred when updating the approle",
        "error" : {
          "response" : response.text
        }
      }

    return self.request_contents
