import approle_engine as ae
import k8s_engine as k8s
from datetime import datetime

class auth_processor:

  def __init__(self, request_contents, template_path):
    self.request_contents = request_contents
    self.template_path = template_path


  def process_auth_request(self):

    if self.request_contents['request_data']['engine'].lower() == "approle":

      # self.request_contents['request_data']['approle_data'] = ae.check_request(self.request_contents['request_data']['approle_data'])
      new_authenticator = ae.Approle(self.request_contents, self.template_path)
      return  new_authenticator.take_action()
      
    elif self.request_contents['request_data']['engine'].lower() == "userpass":
      print("USERPASS")

    elif self.request_contents['request_data']['engine'].lower() in ["kubernetes", "k8s"]:
      new_authenticator = k8s.K8S(self.request_contents, self.template_path)
      return new_authenticator.take_action()

    else:
      run_time = datetime.now()
      self.request_contents['response'] = {
        "result" : "not applied",
        "success" : False,
        "error" : {
          "response" : "The Auth Engine does not have a solution in place"
        },
        "message" : "Program cannot handle auth engines of type " + self.request_contents['request_data']['engine'],
        "run_date" : run_time.strftime("%Y_%m_%d-%H:%M:%S")
      }

    return self.request_contents
