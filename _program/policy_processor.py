import policy_engine as pe

class policy_processor:

  def __init__(self, request_contents, template_path):
    self.request_contents = request_contents
    self.template_path = template_path


  def process_policy_request(self):

    new_policy = pe.Policy(self.request_contents, self.template_path)
    return new_policy.take_action()