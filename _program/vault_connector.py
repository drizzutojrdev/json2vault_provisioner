import requests
import getpass
import json


class vault_connection:

  def __init__(self, credentials={}):

    self.credentials = credentials
    if not bool(self.credentials):
      self.credentials['url'] = input("Please enter the Vault URL > ")
      self.credentials['auth_path'] = input("Please enter the Auth Path > ")
      self.credentials['auth_method'] = input("Please enter the Auth Method > ")

      if self.credentials['auth_method'] == "userpass":
        self.credentials['username'] = input("Please enter the Username > ")
        self.credentials['password'] = getpass.getpass(prompt="Please Enter the Password > ")

      elif self.credentials['auth_method'] == "approle":
        self.credentials['role_id'] = input("Please enter the Role ID > ")
        self.credentials['secret_id'] = getpass.getpass(prompt="Please Enter the Secret ID > ")

    self.token = ""
    self.token_error = "No Error"


  def get_credentials(self):
    credentials = self.credentials.copy()
    return credentials


  def userpass_token(self):

    headers = { 'Content-type' : 'application/json'}
    data = '{"password":"'+self.credentials['password']+'"}'

    url = "/v1/auth/"+self.credentials['auth_path']+"/login/"+self.credentials['username']
    url = url.replace("//", "/")
    url = self.credentials['url'] + url

    response = json.loads(requests.post(url, data=data, headers=headers).content)
    if "errors" not in response.keys():
      self.token = response['auth']['client_token']
      return True
    else:
      print("Error - Vault Token Could not be created with Username and Password")
      self.token_error = response['errors']
      return False


  def approle_token(self):
    headers = { 'Content-type' : 'application/json'}
    data = '{"role_id":"'+self.credentials['role_id']+'","secret_id":"'+self.credentials['secret_id']+'"}'

    url = "/v1/auth/"+self.credentials['auth_path']+"/login"
    url = url.replace("//", "/")
    url = self.credentials['url']+ url

    response = json.loads(requests.post(url, data=data, headers=headers).content)
    if "errors" not in response.keys():
      self.token = response['auth']['client_token']
      return True
    else:
      print("Error - Vault Token Could not be created with Username and Password")
      self.token_error = response['errors']
      return False


  def get_token_error(self):
    return self.token_error


  def generate_token(self):

    if self.credentials['auth_method'] == "userpass":
      return self.userpass_token()
    elif self.credentials['auth_method'] == "approle":
      return self.approle_token()



  def post_call(self, namespace, path, data={}):

    if self.token == "":
      self.generate_token()

    headers = { 'Content-type' : 'application/json', 'X-Vault-Token' : self.token, 'X-Vault-Namepsace' :  namespace}
    data = json.dumps(data)
    url = self.credentials['url']+path

    response = requests.post(url, data=data, headers=headers)

    return response


  def get_call(self, namespace, path):

    if self.token == "":
      self.generate_token()

    headers = { 'Content-type' : 'application/json', 'X-Vault-Token' : self.token, 'X-Vault-Namepsace' :  namespace}
    url = self.credentials['url']+path

    response = requests.get(url, headers=headers)

    return response


  def delete_call(self, namespace, path):

    if self.token == "":
      self.generate_token()

    headers = { 'Content-type' : 'application/json', 'X-Vault-Token' : self.token, 'X-Vault-Namepsace' :  namespace}
    url = self.credentials['url']+path

    response = requests.delete(url, headers=headers)

    return response