from os.path import isfile, join
import json
from datetime import datetime
import ast

import vault_connector as vc

redact_user = False
redact_credentials = False

class Engine:

  def __init__(self, request_contents, template_path):

    self.request_contents = request_contents
    self.template_path = template_path

    self.add_template_list = ["token_policies", "token_bound_cidrs"]


  def check_if_exists(self, engine=""):
    exists = self.vault_connection.get_call(
      self.request_contents['request_data']['namespace'], 
      self.url_path
    )

    if engine == "" or engine in ["policy", "approle"]:
      if exists.status_code == 200:
        return exists.json()
      elif exists.status_code == 404:
        return False
      else:
        print("Error - Something went wrong when checking if the value exists")
        print(exists.status_code)
        return True
    elif engine in ["kvv2"]:
      if exists.status_code == 200:
        return exists.json()
      elif exists.status_code == 403:
        return False
      else:
        print("Error - Something went wrong when checking if the resource exists")
        return True


  def apply_templating_string(self, templated_string):
    run_time = datetime.now()

    for key in self.request_contents['request_data'].keys():
      check_str = "<rd" + "." + key  + ">"
      templated_string = templated_string.replace(check_str, self.request_contents['request_data'][key])

    for key in self.request_contents['request_meta'].keys():
      check_str = "<rm" + "." + key  + ">"
      templated_string = templated_string.replace(check_str, self.request_contents['request_meta'][key])

    templated_string = templated_string.replace("<now>", run_time.strftime("%Y_%m_%d-%H:%M:%S"))
    templated_string = templated_string.replace("<name>", self.request_contents['request_data']['name'])
    return templated_string


  def apply_templating(self, engine="", templated_string=""):

    if engine == "policy":
      return self.apply_templating_string(templated_string)

    if templated_string == "":
      templated_string = self.request_contents['engine_data']

    run_time = datetime.now()
    str_dict = str(templated_string)
    str_dict = self.apply_templating_string(str_dict)
    str_dict = str_dict.replace("'", "\"")
    return ast.literal_eval(str_dict)


  def redact_information(self):

    if redact_user:
      sensitive_cred_list = ["role_id", "secret_id", "username", "password"]
    else:
      sensitive_cred_list = ["secret_id", "password"]

    for sensitive_cred in self.request_contents['credentials'].keys():
      if sensitive_cred in sensitive_cred_list:
        self.request_contents['credentials'][sensitive_cred] = "******"

    if redact_credentials:
      self.request_contents['credentials'] = "******"



  def check_template(self):
    
    if isfile(join(self.template_path, self.request_contents['request_data']['template'])):
      template_data = json.load(open(join(self.template_path, self.request_contents['request_data']['template'])))

      for key in self.request_contents['engine_data'].keys():
        if key in self.add_template_list:
          template_data[key] = self.request_contents['engine_data'][key] + template_data[key]
        else:
          template_data[key] = self.request_contents['engine_data'][key]

      self.request_contents['engine_data'] = template_data

      return True
    else:
      run_time = datetime.now()
      self.request_contents['response'] = {
        "result" : "not applied",
        "success" : False,
        "error" : {
          "response" : "template " + self.request_contents['request_data']['template'] + " not found in " + self.template_path
        },
        "message" : "Template Error",
        "run_date" : run_time.strftime("%Y_%m_%d-%H:%M:%S")
      }

    return False



  def take_action(self):

    if self.request_contents['action'].lower() == "create":
      self.request_contents = self.create()
    elif self.request_contents['action'].lower() == "update":
      self.request_contents = self.update()
    elif self.request_contents['action'].lower() == "delete":
      self.request_contents= self.delete()
    else:
      run_time = datetime.now()
      self.request_contents['response'] = {
        "applied" : False,
        "success" : False,
        "error" : {
          "response" : "action must be set to create, update, or delete"
        },
        "message" : "Invalid action type : " + self.request_contents['action'],
        "run_date" : run_time.strftime("%Y_%m_%d-%H:%M:%S")
      }

    self.redact_information()
    
    return self.request_contents


  def create(self):

    is_valid = self.check_template()

    if not is_valid:
      return self.request_contents

    self.request_contents['engine_data'] = self.apply_templating()

    self.vault_connection = vc.vault_connection(self.request_contents['credentials'])
    self.request_contents['credentials'] = self.vault_connection.get_credentials()
    if not self.vault_connection.generate_token():
      run_time = datetime.now()
      self.request_contents['response'] = {
        "applied" : False,
        "success" : False,
        "error" : {
          "vault_message" : self.vault_connection.get_token_error()
        },
        "message" : "Could not generate token",
        "run_date" : run_time.strftime("%Y_%m_%d-%H:%M:%S")
      }
      return self.request_contents

    if self.request_contents['overwrite'] == False:
      if self.check_if_exists():
        run_time = datetime.now()
        self.request_contents['response'] = {
          "applied" : False,
          "success" : True,
          "error" : {},
          "message" : "Resource exists, set overwrite to True to replace current approle",
          "run_date" : run_time.strftime("%Y_%m_%d-%H:%M:%S")
        }
        return self.request_contents

    response = self.vault_connection.post_call(
      self.request_contents['request_data']['namespace'], 
      self.url_path,
      self.request_contents['engine_data']
    )

    self.create_response(response)

    return self.request_contents


  def delete(self):
    self.vault_connection = vc.vault_connection(self.request_contents['credentials'])
    self.request_contents['credentials'] = self.vault_connection.get_credentials()
    if not self.vault_connection.generate_token():
      run_time = datetime.now()
      self.request_contents['response'] = {
        "applied" : False,
        "success" : False,
        "error" : {
          "vault_message" : self.vault_connection.get_token_error()
        },
        "message" : "Could not generate token",
        "run_date" : run_time.strftime("%Y_%m_%d-%H:%M:%S")
      }
      return self.request_contents

    exists = self.check_if_exists()

    if exists:

      response = self.vault_connection.delete_call(
        self.request_contents['request_data']['namespace'], 
        self.url_path
      )
      
      self.delete_response(response)

    else:
      run_time = datetime.now()
      self.request_contents['response'] = {
        "applied" : False,
        "success" : False,
        "error" : {},
        "message" : "Resource not Found",
        "run_date" : run_time.strftime("%Y_%m_%d-%H:%M:%S")
      }

    return self.request_contents


  def create_response(self, response):
    run_time = datetime.now()

    if response.text == "":
      self.request_contents['response'] = {
        "applied" : True,
        "run_date" : run_time.strftime("%Y_%m_%d-%H:%M:%S"),
        "success" : True,
        "message" : "Created Resource",
        "error" : {}
      }

    else:
      print("Error - Could not create resource")
      self.request_contents['response'] = {
        "applied" : False,
        "run_date" : run_time.strftime("%Y_%m_%d-%H:%M:%S"),
        "success" : False,
        "message" : "An Error occurred when creating the resource",
        "error" : {
          "vault_response" : response.text
        }
      }


  def delete_response(self, response):
    run_time = datetime.now()
    if response.text == "":
      self.request_contents['response'] = {
        "applied" : True,
        "run_date" : run_time.strftime("%Y_%m_%d-%H:%M:%S"),
        "success" : True,
        "message" : "Resource Deleted",
        "error" : {}
      }

    else:
      self.request_contents = {
        "applied" : False,
        "run_date" : run_time.strftime("%Y_%m_%d-%H:%M:%S"),
        "success" : False,
        "message" : "An Error occurred when deleting the resource",
        "error" : {
          "vault_response" : response.text
        }
      }

