from os.path import isfile, join
import json
from datetime import datetime
import ast

import engine as engine
import vault_connector as vc

class SSH(engine.Engine):

  def __init__(self, request_contents, template_path):

    super().__init__(request_contents, template_path)

    self.url_path = "/v1/"+self.request_contents['request_data']['path']+"/roles" +"/"+self.request_contents['request_data']['name']
    self.url_path = self.url_path.replace("//", "/")

    if "template" not in self.request_contents['request_data'].keys():
      self.request_contents['request_data']['template'] = "default_ssh_role.json"


  def check_template(self):
    if isfile(join(self.template_path, self.request_contents['request_data']['template'])):
      template_data = json.load(open(join(self.template_path, self.request_contents['request_data']['template'])))

      for key in self.request_contents['engine_data'].keys():
        if key in ["cidr_list", "exclude_cidr_list", "allowed_users", "allowed_domains", "key_option_specs", "allowed_critical_options", "allowed_extensions"]:
          template_data[key] = template_data[key] + "," + self.request_contents['engine_data'][key] 
        elif key in ["default_critical_options", "default_extensions", "allowed_user_key_lengths"]:
          template_data[key] = self.request_contents['engine_data'][key]
        else:
          template_data[key] = self.request_contents['engine_data'][key]

      self.request_contents['engine_data'] = template_data

      return True
    else:
      run_time = datetime.now()
      self.request_contents['response'] = {
        "result" : "not applied",
        "success" : False,
        "error" : {
          "response" : "template " + self.request_contents['request_data']['template'] + " not found in " + self.template_path
        },
        "message" : "Template Error",
        "run_date" : run_time.strftime("%Y_%m_%d-%H:%M:%S")
      }

    return False