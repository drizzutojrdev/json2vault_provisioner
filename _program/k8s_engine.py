from os.path import isfile, join
import json
from datetime import datetime
import ast

import engine as engine
import vault_connector as vc

class K8S(engine.Engine):

  def __init__(self, request_contents, template_path):

    super().__init__(request_contents, template_path)

    self.url_path = "/v1/auth/"+self.request_contents['request_data']['path']+"/role/"+self.request_contents['request_data']['name']
    self.url_path = self.url_path.replace("//", "/")

    self.add_template_list = self.add_template_list + ["bound_service_account_names", "bound_service_account_namespaces"]

    if "template" not in self.request_contents['request_data'].keys():
      self.request_contents['request_data']['template'] = "default_k8s_role.json"