from os.path import isfile, join
import json
from datetime import datetime
import ast

import engine as engine
import vault_connector as vc

class Policy(engine.Engine):

  def __init__(self, request_contents, template_path):

    super().__init__(request_contents, template_path)

    self.url_path = "/v1/sys/policies/acl/"+self.request_contents['request_data']['name']
    self.url_path = self.url_path.replace("//", "/")

    if "template" not in self.request_contents['request_data'].keys():
      self.request_contents['request_data']['template'] = "default_policy.json"


  def check_template(self):
    if isfile(join(self.template_path, self.request_contents['request_data']['template'])):
      template_data = json.load(open(join(self.template_path, self.request_contents['request_data']['template'])))

      self.request_contents['engine_data']['permissions'] = self.request_contents['engine_data']['permissions'] + template_data['permissions']

      for key in template_data['header'].keys():
        self.request_contents['engine_data']['header'][key] = template_data['header'][key]

      return True
    else:
      run_time = datetime.now()
      self.request_contents['response'] = {
        "result" : "not applied",
        "success" : False,
        "error" : {
          "response" : "template " + self.request_contents['request_data']['template'] + " not found in " + self.template_path
        },
        "message" : "Template Error",
        "run_date" : run_time.strftime("%Y_%m_%d-%H:%M:%S")
      }

    return False


  def create(self):

    is_valid = self.check_template()

    if not is_valid:
      return self.request_contents

    self.vault_connection = vc.vault_connection(self.request_contents['credentials'])
    self.request_contents['credentials'] = self.vault_connection.get_credentials()
    if not self.vault_connection.generate_token():
      run_time = datetime.now()
      self.request_contents['response'] = {
        "result" : "not applied",
        "success" : False,
        "error" : {
          "vault_message" : self.vault_connection.get_token_error()
        },
        "message" : "Could not generate token",
        "run_date" : run_time.strftime("%Y_%m_%d-%H:%M:%S")
      }
      return self.request_contents

    if self.request_contents['overwrite'] == False:
      if self.check_if_exists():
        run_time = datetime.now()
        self.request_contents['response'] = {
          "result" : "not applied",
          "success" : True,
          "error" : {},
          "message" : "Policy exists, set overwrite to True to replace current policy",
          "run_date" : run_time.strftime("%Y_%m_%d-%H:%M:%S")
        }
        return self.request_contents

    the_policy = ""

    for key in self.request_contents['engine_data']['header'].keys():

      header_line = "# " + key + " : " + self.request_contents['engine_data']['header'][key]
      the_policy = the_policy + header_line + "\n"

    the_policy = the_policy + "\n" # space between meta and paths

    for permission in self.request_contents['engine_data']['permissions']:

      if "header" in permission.keys():
        the_policy = the_policy + "### " + permission['header'] + " ###\n\n"

      path_block = 'path "' + permission['path'] + '" {\n'
      path_permissions = "\tcapabilities = ["

      for capability in permission['capabilities']:
        path_permissions = path_permissions + '"' + capability + '",' 

      path_permissions = path_permissions + ']\n'
      path_block = path_block + path_permissions
      path_block = path_block + "}\n\n"

      the_policy = the_policy + path_block

    the_policy = self.apply_templating("policy", the_policy)

    response = self.vault_connection.post_call(
      self.request_contents['request_data']['namespace'], 
      self.url_path,
      {"policy":the_policy}
    )

    self.create_response(response)
    
    return self.request_contents


  def update(self):
    return self.create()