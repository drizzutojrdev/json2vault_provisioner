import argparse
import logging
from os import listdir
from os.path import isfile, join
import json
import sys

import request_processor as rp

logger = logging.getLogger(__name__)


def check_keys(request_contents):

  key_test = "action"

  if key_test not in request_contents.keys():
    print("Error - " + key_test + " key missing")
    return False
  elif type(request_contents[key_test]) is not str:
    print("Error - "+ key_test + " is not a string ")
    return False
  elif request_contents[key_test].lower() not in ["create", "update", "delete"] :
    print("Error - " + key_test + " not valid option")
    print("Valid option are create, update, delete")
    return False

  key_test = "overwrite"

  if key_test not in request_contents.keys():
    print("Error - " + key_test + " key missing")
    return False
  elif type(request_contents[key_test]) is not bool:
    print("Error - "+ key_test + " is not a boolean ")
    return False

  key_test = "type"

  if key_test not in request_contents.keys():
    print("Error - " + key_test + " key missing")
    return False
  elif type(request_contents[key_test]) is not str:
    print("Error - "+ key_test + " is not a string ")
    return False
  elif request_contents[key_test] not in ["policy", "secret", "auth"] :
    print("Error - " + key_test + " not valid option")
    print("Valid option are policy, secret, auth")
    return False

  key_test = "request_data"

  if key_test not in request_contents.keys():
    print("Error - " + key_test + " key missing")
    return False
  elif type(request_contents[key_test]) is not dict:
    print("Error - "+ key_test + " is not a json object")
    return False

  key_test = "request_meta"

  if key_test not in request_contents.keys():
    print("Error - " + key_test + " key missing")
    return False
  elif type(request_contents[key_test]) is not dict:
    print("Error - "+ key_test + " is not a json object")
    return False

  return True


def validate_request(file_path):

  if not isfile(file_path):
    print("Error - Request not a File")
    return False

  try:
    request_contents = json.load(open(file_path))
  except ValueError:
    logger.error("Error - Request not valid Json")
    return False

  if type(request_contents) is dict:
    return check_keys(request_contents)
  elif type(request_contents) is list:
    for request in request_contents:
      if not check_keys(request):
        return False
  else:
    print("Error - Json object is not a list or dictionary")
    return False


  return True


if __name__ == "__main__":

  parser = argparse.ArgumentParser()
  parser.add_argument("-r", "--request", help = 'Path to Look for New Requests', required=True)
  parser.add_argument("-o", "--report", help = 'Path to the Report')
  parser.add_argument("-c", "--config", help = 'Path to the Config File')
  parser.add_argument("-t", "--template", help = 'Path to the Template Directory')
  parser.add_argument("-s", "--settings", help = 'Pass this flag to enter settings (coming soon)', action='store_true')
  parser.add_argument("-d", "--debug", action='store_true', help = 'Run this command in Debug')
  args = parser.parse_args() 

  request_path = "."
  report_path = "./_reports"
  config_path = ""
  template_path = "./_templates"

  if args.request:
    request_path = args.request

  if args.report:
    report_path = args.report

  if args.config:
    config_path = args.config

  if args.template:
    template_path = args.template

  valid_request = validate_request(request_path)

  if not valid_request:
    sys.exit(1)
  else:
    processor = rp.processor(request_path, report_path, args.debug, template_path, config_path)
    processor.process_requests()