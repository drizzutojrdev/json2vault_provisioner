import json
from os.path import split, splitext, isfile
import os
from datetime import datetime
import sys

import auth_processor as ap
import policy_processor as pp
import secret_processor as sp

class processor:

  def __init__(self, request_path, report_path, debug, template_path, config_path=""):
    self.request_path = request_path
    self.report_path = report_path
    self.template_path = template_path
    self.debug = debug
    self.config_path = config_path

    self.request_contents = json.load(open(self.request_path))

    self.process_data()


  def process_data(self):

    if type(self.request_contents) is list:
      self.request_list = self.request_contents
    elif type(self.request_contents) is dict:
      self.request_list = []
      self.request_list.append(self.request_contents)


  def process_requests(self):

    response_list = []

    for request in self.request_list:

      if self.config_path != "":
        if not isfile(self.config_path):
          print("Error - Config not a File")
          sys.exit(1)
        else:
          try:
            request['credentials'] = json.load(open(self.config_path))
          except ValueError:
            logger.error("Error - Config not valid Json")
            sys.exit(1)

      if request['type'].lower() == "auth":
        auth_request = ap.auth_processor(request, self.template_path)
        request_response = auth_request.process_auth_request()
        response_list.append(request_response)

      elif request['type'].lower() == "secret":
        secret_request = sp.secret_processor(request, self.template_path)
        request_response = secret_request.process_secret_request()
        response_list.append(request_response)

      elif request['type'].lower() == "policy":
        policy_request = pp.policy_processor(request, self.template_path)
        request_response = policy_request.process_policy_request()
        response_list.append(request_response)

      else:
        print("Error - Invalide type")

    head, tail = split(self.request_path)
    file_name = splitext(tail)[0]

    run_time = datetime.now()
    report_file = self.report_path + "/" + run_time.strftime("%Y_%m_%d-%H:%M:%S") + "-" + tail

    if not os.path.exists(self.report_path):
      os.makedirs(self.report_path)

    with open(report_file, 'w+') as out:
      out.write(json.dumps(response_list, indent=2))




# def process_approle_request(request_contents, request_path, file, success_path):
#   new_approle = approle.approle(request_contents, request_path, file, success_path)
#   approle_response = new_approle.take_action()
#   return approle_response


# def process_request(request_path, file, success_path):

#   request_contents = json.load(open(join(request_path, file)))

#   print(file + " : Consumer Type : " + request_contents['consumer_type'])

#   is_applied = False
#   if request_contents['consumer_type'] == 'approle':
#     request_contents['request_response'] = process_approle_request(request_contents, request_path, file, success_path)
#   else:
#     run_time = datetime.now()
#     request_contents['request_response'] = {
#     "date_applied" : run_time.strftime("%Y_%m_%d-%H:%M:%S"),
#     "success" : "failure",
#     "message" : "consumer_type does not have process to create"
#   }

#   print(request_contents['request_response'])


#   new_folder = success_path + "/_"+splitext(file)[0]
#   if not os.path.exists(new_folder):
#     os.makedirs(new_folder)

#   new_file = request_contents['request_response']['date_applied']+'-'+request_contents['action']+".json"

#   with open(join(new_folder, new_file), 'w+') as out:
#     out.write(json.dumps(request_contents, indent=2))
