[
  {
    "action" : "create",
    "overwrite" : true,
    "type" : "auth",
    "credentials" : {},
    "request_info" : {
      "engine" : "approle",
      "namespace" : "root",
      "path" : "approle",
      "name" : "test",
      "approle_data" : {
        "bind_secret_id" : true,
        "secret_id_bound_cidrs" : [],
        "secret_id_num_uses" : 0,
        "secret_id_ttl" : "",
        "enable_local_secret_ids" : false,
        "token_ttl" : "",
        "token_max_ttl" : "",
        "token_policies" : ["test"],
        "token_bound_cidrs" : [],
        "token_explicit_max_ttl" : "",
        "token_no_default_policy" : false,
        "token_num_uses" : 0,
        "token_period" : "",
        "token_type" : ""
      }
    },
    "request_meta" : {}
  },
  {
    "action" : "create",
    "overwrite" : true,
    "type" : "secret",
    "credentials" : {},
    "request_info" : {
      "engine" : "kv-v2",
      "namespace" : "root",
      "path" : "kv",
      "name" : "test",
      "secret_data" : {
        "path" : "/sub1/sub2/my_secret",
        "key" : "test"
      }
    },
    "request_meta" : {}
  }
]