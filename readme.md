# Welcome to json2vault_provisioner

This tool is used for provisioning in vault, via JSON files and a python 3 program.  JSON files are created to submit requests, which are then formatted and submitted to the program.  The program creates/updates/deletes the resource and then generates a report.  This report contains the request and the response for the server.  These reports should be saved to track what provisioning requests have been submitted to your Vault Instance.

# The Program

The program is located inside the _programs folder.  The file to run is provisioner.py.  There are some required and optional arguments you can pass.

## Command Line Arguments

-r --request (required) is the path to the requeset json file.  
-o --report (required) is the path that the report will be generated in.  
-c --config (optional) is the path to the config file.  The Configuration details can either exist inside the request or inside the config file.  
-t --template (optional) is the path to the custom template directory.  If you wish to store your templates elsewhere, you can point the program to look here.  
-s --settings (optional) is the settings option.  IN PROGRESS.  
-d --debug (optional) is the debug flag.  Set this flag to run in debug mode. IN PROGRESS.  

## Example Run

```bash
python provisioner.py -r ../_resources/_requests/approle-create-example.json -o ../_reports -c ../_resources/credentials.json
```

# Requests

Requests are JSON files created to provision inside Vault.  These files can either be created manually or via forms/automation.  

A JSON request can create multiple resources or only one resource.  If you are creating one resource, then simply make your file a JSON array.  If you are creating multiple requests inside one JSON request file, then a JSON list should be created with the multiple requests inside it.  

There are four important sections:  request data, request_info data, engine data, and request_meta. 

For example requests, please see the files located in _requests.  

## Request Data

The Request Data is the information stored inside the "first level" of the JSON object.  

action (required) create/update/delete - what type of action you want to create.  
overwrite (required) boolean - do you want to overwrite the data inside vault if it already exists.  
type (required) auth/secret/policy - what type of resource are you trying to create.  
credentials (optional) object - What are the credentials for the program to use to fufill this request
request_info (required) object - See next section

## Request Info Data

The Request Info Data is an object inside the Request Data that houses infomration dependent on the type of request you are making.  

Shared among all types  
namespace (required)  - the namespace (or namespace path) that the engine or policy exists in.  
name (required) - the name of the authenticator you are creating.  
template (optional) - the tempalte you want to use for this auth engine or policy.  

type = auth  
path (required) - the path to the auth engine inside the namespace.  
eninge (required) - the type of engine you are creating hte authenticator in.
auth_data (required) - See next section.  

type = secret  
path (required) - the path to the auth engine inside the namespace.  
eninge (required) - the type of engine you are creating hte authenticator in.
secret_data (required) - See next section.  

type = policy  
policy_data (required) - See next section

## Engine Data

The engine data (auth_data, secret_data, policy_data) is dependent on the type and potential engine.  To see the values for this, please look at the examples inside _program/_templates.  

# Templates

You may create custom templates to apply to your program so that when you create a resource, it pulls from that file instead of the default or providing all details inside your Request JSON file.  This helps keeps files smaller and allows you to enforce common resources inside Vault.  

The default template location is _programs/_templates.  You may set your own templates folder using the -t option.  

Please not that the default templates inside _programs_tempaltes should not be edited or changed.  They should be used as reference to create new templates.  If these files are not present and the default template directory is used, your resources will fail to create (or other unexpected behaviour will occur).

